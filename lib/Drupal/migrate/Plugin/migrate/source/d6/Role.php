<?php

/**
 * @file
 * Contains \Drupal\migrate\Plugin\migrate\source\d6\Role.
 */

namespace Drupal\migrate\Plugin\migrate\source\d6;

use Drupal\migrate\Plugin\migrate\source\SqlBase;
use MyProject\Proxies\__CG__\stdClass;

/**
 * Drupal 6 role source.
 *
 * @PluginId("drupal6_role")
 */
class Role extends SqlBase {

  /**
   * {@inheritdoc}
   */
  function query() {
    // Do not attempt to migrate the anonymous or authenticated roles.
    return $this->database->select('role', 'r')
      ->fields('r')
      ->condition('rid', 2, '>');
  }

  /**
   * {@inheritdoc}
   */
  function fields() {
    return array(
      'rid' => 'Role ID',
      'name' => 'Role Name',
    );
  }

  /**
   * This is only here to avoid fatals for un-implemented abstract methods.
   * @todo remove this once getNextRow() is implemented in SqlBase or, if it
   *       turns out we actually need to implement this, do it.
   */
  function getNextRow() {
    return new stdClass();
  }

  /**
   * @todo implement or remove this.
   * @see Role::getNextRow()
   */
  function computeCount() {
    return 0;
  }

  /**
   * @todo implement or remove this.
   * @see Role::getNextRow()
   */
  function performRewind() {
    return new stdClass();
  }

}
