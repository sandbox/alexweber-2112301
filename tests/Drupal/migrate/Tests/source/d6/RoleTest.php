<?php

/**
 * @file
 * Contains \Drupal\migrate\Tests\source\d6\RoleTest.
 */

namespace Drupal\migrate\Tests\source\d6;

use Drupal\migrate\Plugin\migrate\source\d6\Role;
use Drupal\Tests\UnitTestCase;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Database\Connection;

/**
 * Tests role migration from D6 to D8.
 *
 * @group Migrate
 */
class RoleTest extends UnitTestCase {
  /**
   * The tested source plugin.
   *
   * @var \Drupal\migrate\Plugin\migrate\source\d6\Role.
   */
  protected $source;

  /**
   * Database connection to use.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $connection;

  public static function getInfo() {
    return array(
      'name' => 'D6 role source functionality',
      'description' => 'Tests D6 role source plugin.',
      'group' => 'Migrate',
    );
  }

  protected function setUp() {
    $this->connection = $this->getMockBuilder('Drupal\Core\Database\Connection')
      ->disableOriginalConstructor()
      ->getMock();

    // @todo Figure out how Comment::query() is used.
    // @todo create a StatementInterface object with relevant data attached? (or mocks)
    $statement = null;
    $this->connection->expects($this->any())
      ->method('query')
      ->will($this->returnValue($statement));

    $configuration = array();
    $plugin_definition = array();
    $this->cache = $this->getMock('Drupal\Core\Cache\CacheBackendInterface');
    $this->source = new Role($configuration, 'drupal6_role', $plugin_definition, $this->cache, $this->connection);
  }

  /**
   * Tests retrieval.
   */
  public function testRetrieval() {
    $this->source->rewind();
    // @todo mock two rows.
    $expected_data_keys = array('rid', 'name');
    // First row.
    $this->assertTrue($this->source->valid(), 'Valid row found in source.');
    $data_row = $this->source->current();
    foreach ($expected_data_keys as $expected_data_key) {
      $this->assertTrue(isset($data_row[$expected_data_key]), sprintf('Found key "%s" on source data row.', $expected_data_key));
    }
    // Second row.
    $data_row = $this->source->current();
    $this->source->next();
    foreach ($expected_data_keys as $expected_data_key) {
      $this->assertTrue(isset($data_row[$expected_data_key]), sprintf('Found key "%s" on source data row.', $expected_data_key));
    }
  }
}
